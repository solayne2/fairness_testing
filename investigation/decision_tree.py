
from sklearn import tree
import pandas as pd
import random
from sklearn.externals import joblib
import time

def readInDataSet():
    file_encoding = 'utf8' # set file_encoding to the file encoding (utf8, latin1, etc.)
    input_fd = open("./datasets/business_small_loans_US_Only_transformed.csv", encoding=file_encoding, errors = 'backslashreplace')

    return pd.read_csv(
        input_fd
    )

def cuteLittleHelper():
    df = readInDataSet()

    df.reindex_axis(sorted(df.columns), axis=1)

    features = list(df.columns[:len(df.columns)-1])

    stringToPrint = []
    for feature in features:
        stringToPrint.append("<input>\n")
        stringToPrint.append("\t<name>'{0}'</name>\n".format(feature))
        stringToPrint.append("\t<type>categorical</type>\n")
        stringToPrint.append("\t<values>\n")

        values = df[feature].unique()
        for value in values:
            stringToPrint.append("\t\t<value>'{0}'</value>\n".format(value))

        stringToPrint.append("\t</values>\n")
        stringToPrint.append("</input>\n")
    print("".join(stringToPrint))

def cuteLittleHelper2():
    df = readInDataSet()
    features = list(df.columns[:len(df.columns)-1])

    mapping = {}
    for feature in features:
        values = df[feature].unique()
        mapping[feature] = list(values)

    print(mapping)

    

def main ():
    df = readInDataSet()

    features = list(df.columns[:len(df.columns)-1])
    #features = ['Business Sector', 'City/Town', 'State/Province/Region Name', 'State/Province/Region Code', 'Country Name', 'Region Name', 'Is Woman Owned?', 'Is First Time Borrower?']

    df_with_dummies= pd.get_dummies(
        df,
        columns=features,
        drop_first=False
    )

    y = df_with_dummies['Loan Value Greater']
    features = list(df_with_dummies.columns[:len(df_with_dummies.columns)-1])
    X = df_with_dummies[features]

    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X, y)

    #joblib.dump(clf, '{0}_decision_tree.joblib'.format(int(time.time()))) 


    numAssignments = 1
    #assignments = [[random.choice([0,1]) for f in features] for _ in range(numAssignments)]
    assignments = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1]]
    predictions = clf.predict(assignments)
    print(predictions)

main()
#cuteLittleHelper2()