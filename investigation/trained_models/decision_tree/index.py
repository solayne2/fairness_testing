#!/usr/bin/env python -W ignore::DeprecationWarning
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys
import pandas as pd
from sklearn.externals import joblib
import os
from options import optionsMap, possibleOptions


dir_path = os.path.dirname(os.path.realpath(__file__))

columnNames = ['Business Sector', 'City/Town', 'State/Province/Region Name', 'State/Province/Region Code', 'Country Name', 'Region Name', 'Is Woman Owned?', 'Is First Time Borrower?']
clf = joblib.load(os.path.join(dir_path, 'model.joblib'))
def generateDataFrameFromArgs(args):
    pO = possibleOptions
    pO.append(args)
    return pd.DataFrame(
        data=pO,
        columns=columnNames
    )

def main():
    args = sys.argv[1:]
    df = generateDataFrameFromArgs(args)

    df.reindex(sorted(df.columns), axis=1)

    df_with_dummies= pd.get_dummies(
        df,
        columns=columnNames,
        drop_first=False
    )

    lastRow = None
    for index, row in df_with_dummies.iterrows():
        lastRow = row

    assignment = [list(lastRow)]

    prediction = clf.predict(assignment)

    print(prediction[0])

if __name__ == "__main__":
    main()

